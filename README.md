# Introduction #
This [git](http://git-scm.com/) repo is used to host [Maven](http://maven.apache.org/) artifacts. You can host this repo on the web instead of installing your own [Open Source Nexus Server](http://www.sonatype.org/nexus/). The instructions are loosely based on [this post](http://cemerick.com/2010/08/24/hosting-maven-repos-on-github).


# How to Add Artifacts #
The basic steps are:

1. Add artifacts.
2. Commit your changes.
3. Push your changes to a remote git server accessible by developers working on the project.

## Existing Maven Project ##
You can deploy an existing project to your local repo using the command below. This is expecially useful if you have in-house forks.

    mvn deploy -DaltDeploymentRepository=<repo.id>::default::file://<absolute_path>/<releases|snapshots> 

## Local .jar ##
To deploy a local jar file that doesn't use maven, you can execute the following command and replacing the values where appropriate.

    mvn deploy:deploy-file -DgroupId=<group-id> \
      -DartifactId=<artifact-id> \
      -Dversion=<version> \
      -Dpackaging=jar \
      -Dfile=<absolute-path-to-file> \
      -DrepositoryId=<repo.id> \
      -Durl=file://<absolute_path>/<releases|snapshots>

# Examples #
An example of deploying an existing maven project.

    mvn deploy -DaltDeploymentRepository=bobsrepo::default::file:///Users/bob/Desktop/mvn-repo/snapshots

An example of deploying a non maven jar file.

    mvn deploy:deploy-file -DgroupId=com.google.android \
      -DartifactId=analytics \
      -Dversion=2.0-beta5 \
      -Dpackaging=jar \
      -Dfile=/Users/bob/Downloads/analytics-2.0-beta5.jar \
      -DrepositoryId=bobsrepo \
      -Durl=file:///Users/bob/Desktop/mvn-repo/releases